import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBeaconPage } from './edit-beacon.page';

describe('EditBeaconPage', () => {
  let component: EditBeaconPage;
  let fixture: ComponentFixture<EditBeaconPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBeaconPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBeaconPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
