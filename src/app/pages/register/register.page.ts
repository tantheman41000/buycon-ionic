import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { RouterLink } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(public alertController: AlertController) { }

  ngOnInit() {
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'PLEASE CHECK',
      subHeader: 'Here are your information',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }

}
