import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { MainPage } from '../main/main.page';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.page.html',
  styleUrls: ['./view-order.page.scss'],
})
export class ViewOrderPage implements OnInit {

 
  data
  
  
  constructor(private navCtrl:NavController,private http:HttpClient,private route:ActivatedRoute) {
    
   }

  ngOnInit() {
    this.route.params.subscribe( params =>
      {
        this.http.get(`http://localhost:8080/dish/${params['name']}`).subscribe(res => {
          console.log(res) 
          return this.data=res
        })
      }
  )
    
  }

 

}
