import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import {ViewOrderPage} from '../view-order/view-order.page'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OrderService } from 'src/app/services/order.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {

  url = "http://localhost:8080/restaurants"
  data
  constructor(private navCtrl:NavController,private http:HttpClient,private router: Router) {
  
   }
   
  ngOnInit() {
    this.http.get(this.url).subscribe(res => {
      console.log(res) 
      return this.data=res
    })
  }

}


