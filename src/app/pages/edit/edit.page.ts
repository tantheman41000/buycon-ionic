import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { IBeacon } from '@ionic-native/ibeacon/ngx';


import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  data
  beacon
  

  constructor(private navCtrl:NavController,private http:HttpClient,private route:ActivatedRoute,private ibeacon: IBeacon) { }

  ngOnInit() {
    this.route.params.subscribe( params =>
      {
        this.http.get(`http://localhost:8080/dish/${params['name']}`).subscribe(res => {
          console.log(res) 
          return this.data=res
        })
      }
  )

  this.route.params.subscribe( params =>
    {
      this.http.get(`http://localhost:8080/beacons/restName/${params['name']}`).subscribe(res => {
        console.log(res) 
        return this.beacon=res
      })
    }
)
  }

  ionViewWillEnter(){

     // Request permission to use location on iOS
     this.ibeacon.requestAlwaysAuthorization();
     // create a new delegate and register it with the native layer
     let delegate = this.ibeacon.Delegate();
  
     // Subscribe to some of the delegate's event handlers
     delegate.didRangeBeaconsInRegion()
       .subscribe(
         data => console.log('didRangeBeaconsInRegion: ', data),
         error => console.error()
       );
     delegate.didStartMonitoringForRegion()
       .subscribe(
         data => console.log('didStartMonitoringForRegion: ', data),
         error => console.error()
       );
     delegate.didEnterRegion()
       .subscribe(
         data => {
           console.log('didEnterRegion: ', data);
         }
       );
  
     let beaconRegion = this.ibeacon.BeaconRegion('Thai Smile','FDA50693-A4E2-4FB1-AFCF-C6EB07647825');
  
     this.ibeacon.startMonitoringForRegion(beaconRegion)
       .then(
         () => console.log('Native layer received the request to monitoring'),
         error => console.error('Native layer failed to begin monitoring: ', error)
       );
  }
  

}
