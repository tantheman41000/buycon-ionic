import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-restaurant-choose-form',
  templateUrl: './restaurant-choose-form.page.html',
  styleUrls: ['./restaurant-choose-form.page.scss'],
})
export class RestaurantChooseFormPage implements OnInit {

  url = "http://localhost:8080/restaurants"
  data

  constructor(private navCtrl:NavController,private http:HttpClient,private router: Router) { }

  ngOnInit() {
    this.http.get(this.url).subscribe(res => {
      console.log(res) 
      return this.data=res
    })
  }

}
