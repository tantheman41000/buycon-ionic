import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RestaurantChooseFormPage } from './restaurant-choose-form.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurantChooseFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RestaurantChooseFormPage]
})
export class RestaurantChooseFormPageModule {}
