import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
export class OrderService {
  
  restaurants: []
  url = 'http://localhost:8080/restaurants'
  dishUrl = 'http://localhost:8080/dish/Vietnam%20smile'
  

  constructor(private http:HttpClient) { }

  searchData(name: string): Observable<any> {
      return this.http.get(`${this.url}?s=${encodeURI(name)}`).pipe(
        map(results => {
          console.log('RAW: ', results)
          return results['Search']
        })
      )
    }
  getDetails(name){
    return this.http.get(`${this.url}?i=${name}`)
    
  }
}

