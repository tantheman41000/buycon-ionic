import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'users', pathMatch: 'full' },
  { path: 'main', loadChildren: './pages/main/main.module#MainPageModule' },
  { path: 'view-order/:name', loadChildren: './pages/view-order/view-order.module#ViewOrderPageModule' },
  { path: 'users', loadChildren: './pages/users/users.module#UsersPageModule' },
  { path: 'restaurant-choose-form', loadChildren: './pages/restaurant-choose-form/restaurant-choose-form.module#RestaurantChooseFormPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'edit/:name', loadChildren: './pages/edit/edit.module#EditPageModule' },
  { path: 'register-beacon', loadChildren: './pages/register-beacon/register-beacon.module#RegisterBeaconPageModule' },
  { path: 'edit-beacon', loadChildren: './pages/edit-beacon/edit-beacon.module#EditBeaconPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
